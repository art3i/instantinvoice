if (Meteor.isClient) {


  var maxItemIndex= new ReactiveVar(0);

  var currentUserID = new ReactiveVar(0);
  var currentInvoiceID = new ReactiveVar(0);

  var currentSubtotal=new ReactiveVar(0);
  var currentDiscount = new ReactiveVar(0);
  var currentTax = new ReactiveVar(0);


Template.app.rendered = function (){

    Tracker.autorun(function(){

    //Meteor.subscribe("itemCounterDB", Meteor.userId());
    Meteor.subscribe("itemsDB");

    Meteor.subscribe("images");
    Meteor.subscribe("invoiceDB");

  });



// ------------- Generating Random User ID & Invoice ID for guest user -----------------------------

if(!Meteor.userId) {
//  $('#userID').text( (Math.floor(Math.random() * 1e7) + new Date().getMilliseconds().toString(36)).toUpperCase() ) ;

currentUserID.set( Random.id(7) );  // Random.id is a built-in meteor function
currentInvoiceID.set( Random.id(4) );

  $('#userID').text(currentUserID.get()) ;

  $('#invoiceID').text(currentInvoiceID.get());

  // creating initial itemIndex = 1
  maxItemIndex.set(1);


  // --------------- creating a dummy invoice db ----------


var idata = {
  invoiceOwnerID  : currentUserID.get(),
    invoiceID     : currentInvoiceID.get(),

  ownerAddress      : "",
  clientAddress     : "",

  logoID            : "",
  invoiceNumber     : "",
  dateCreated       : "",
  dueDate           : "",
  discount          : "",
  discountType      : "",
  tax               : "",
  taxType           : " ",

  subtotal          : 0,
  totalAmount       : 0,
  paidAmount        : 0,
  balanceDue        : 0,
  notes             : "",
  terms             : "",
}

Meteor.call('addInvoice', idata);
// ------------- add initial dummy items to db -----------------

  var data= {
              invoiceOwnerID  : currentUserID.get(),
                invoiceID     : currentInvoiceID.get(),
                itemIndex     : maxItemIndex.get(),
                quantity      : 1,
                rate          : 0,
                amount        : 0.00,
                itemTitle     : "",

              };
Meteor.call('addItem', data );




}  //end if user logged in check

// -------------------------- END block for guest user ---------------------

} ;

//-------------------------------------------------------------------------
// ----- END template.rendered ------------------------
//-------------------------------------------------------------------------

  Template.app.helpers({

    itemCounter: function () {

      return Items.find(

            { $and:[ {"invoiceID": currentInvoiceID.get()},

                    {"invoiceOwnerID": currentUserID.get() },

                    {"invoiceOwnerID" : {'$exists':true} } ] }

              );

    },

    getInvoice: function () {

      return Invoice.find(

            { $and:[ {"invoiceID": currentInvoiceID.get()},

                    {"invoiceOwnerID": currentUserID.get() },

                    {"invoiceOwnerID" : {'$exists':true} } ] }

              );

    },


  });

  // -------------------------------------------------------------------
  // ---------------------END Template.app.helpers -----------------------
  // -------------------------------------------------------------------

  Template.app.events({

// ----- event for updating owner address ------------

'blur #ownerAddress' : function(event, tmpl){

      var invoiceDBid=$('#invoiceDBid').val();
      var ownerAddress=$('#ownerAddress').val();

     // now for formating purpose; replacing the new line charecter with the html equivalent of \n
    //  ownerAddress=ownerAddress.replace(/\r\n|\r|\n/g, "&#13;&#10;")

      var data = {
                invoiceDBid : invoiceDBid,
                 ownerAddress : ownerAddress,
      }

Meteor.call('updateOwnerAddress', data );

},

// ------- END updating owner address

// ----- event for updating client address ------------

'blur #clientAddress' : function(event, tmpl){

      var invoiceDBid=$('#invoiceDBid').val();
      var clientAddress=$('#clientAddress').val();
      var data = {
                invoiceDBid : invoiceDBid,
                 clientAddress : clientAddress,
      }

Meteor.call('updateClientAddress', data );

},

// ------- END updating client address

// ----- event for updating invoiceNumber ------------

'blur #invoiceNumber' : function(event, tmpl){

      var invoiceDBid=$('#invoiceDBid').val();
      var invoiceNumber=$('#invoiceNumber').val();

      var data = {
                invoiceDBid : invoiceDBid,
                 invoiceNumber : invoiceNumber,
      }

Meteor.call('updateInvoiceNumber', data );

},

// ------- END updating invoiceNumber

// ----- event for updating date ------------

// --------- rendering datetimepicker to select date from calender ----------
'focus .datetimepicker, click .datetimepicker':function(event,tmpl){

        $('.datetimepicker').datetimepicker({
            format: 'MMMM DD, YYYY', //July 06, 2015
                  });
},

// ------- END of datetimepicker rendering ----------------------------

'blur #date-created' : function(event, tmpl){

      var invoiceDBid=$('#invoiceDBid').val();
      var dateCreated=$('#date-created').val();
      var data = {
                invoiceDBid : invoiceDBid,
                 dateCreated : dateCreated,
      }

Meteor.call('updateDateCreated', data );

},

'blur #due-date' : function(event, tmpl){

      var invoiceDBid=$('#invoiceDBid').val();
      var dueDate=$('#due-date').val();
      var data = {
                invoiceDBid : invoiceDBid,
                 dueDate : dueDate,
      }

Meteor.call('updateDueDate', data );

},

// ------- END updating date ----------------------------


// ---------------------------------------------------------------------
// --------- event handler for add more item button --------------
//-------------------------------------------------------------------------
    'click .itemAddBtn': function (event, tmpl){

     event.preventDefault();

          // setting indexCounter and new index order
          var indexCounter=1;
             $('.item-index').each(function(){
                   indexCounter=indexCounter+1;
             });
                   maxItemIndex.set( indexCounter );
          // END adjusting index

      var data= {
                  invoiceOwnerID  : currentUserID.get(),
                    invoiceID     : currentInvoiceID.get(),
                    itemIndex     : maxItemIndex.get(),
                    quantity      : 1,
                    rate          : 0,
                    amount        : 0,
                    itemTitle     : "",

                  };


 Meteor.call('addItem', data );


     },

    //now update invoice data to the database when user add unit & rate data
    //then calculate each item amount , store & return live

  'keyup .itemTracker, blur .item-name' : function (event, tmpl) {

              event.preventDefault();


              var workingID =  $(event.target).attr("itemID")  ; //working means the row (field) i'm calculating currently


              // here,using this conditional select query to filter & thus select only the current editing row
          var workingQuantity = $( "input[itemID="+workingID+"] ").filter( 'input[name="itemQuantity"]' ).val();
          var workingRate = $( "input[itemID="+workingID+"]").filter( 'input[name="itemRate"]' ).val();

          var workingItemTitle= $( "input[itemID="+workingID+"]").filter( 'input[name="itemTitle"]' ).val();
          var workingIndex = $( "input[itemID="+workingID+"]").filter( 'input[name="item-index"]' ).val();

          //client side only calculation when keyup event fires

          var amount= (workingQuantity*workingRate);
          amount=parseFloat(amount);
          amount = amount.toFixed(2);
          amount=Number(amount);

          //displaying amount on each changed item field
          $( "input[itemID="+workingID+"]").filter( 'input[name="itemAmount"]' ).val( amount );


        // ------------------ calculating subtotal ----------------------

          var subtotalVar=0; //temp value
          subtotalVar=Number(subtotalVar);

          $('.itemAmount').each(function(){
                  var subtotalRaw=$(this).val();

                  if( isNaN(subtotalRaw) )
                  {
                    subtotalRaw=0;
                    //console.log("NaN value error");
                  }
                  else{
                    subtotalRaw=Number( parseFloat(subtotalRaw).toFixed(2) );
                    subtotalVar=(subtotalVar+subtotalRaw);
                  //  console.log("subtotalRaw :"+subtotalRaw+" subtotalVar:"+subtotalVar);
                  }

          });

          currentSubtotal.set(subtotalVar);

          $('#subtotal').val(subtotalVar); //displaying subtotal value


          // ---------------- end subtotal calculation -----------------------


          // ----------- end calculations -----------------------

          // -------------------------------------------------------------------
          // Now Update data of current item index, quantity, rate & amount
          // on each ~ keyup .itemTracker ~ event
          // -------------------------------------------------------------------
              var data = {

                        itemID  : workingID,
                invoiceOwnerID  : currentUserID.get(),
                  invoiceID     : currentInvoiceID.get(),
                  itemIndex     : workingIndex,
                  quantity      : workingQuantity,
                  rate          : workingRate,
                  amount        : amount,
                  itemTitle     : workingItemTitle,

              }

              Meteor.call('updateItem', data );

              // updating subtotal db

                    var invoiceDBid=$('#invoiceDBid').val();
                    var dataST = {
                              invoiceDBid : invoiceDBid,
                               subtotal : subtotalVar,
                             }

              Meteor.call('updateSubtotal', dataST );


    }, // ---- END .itemTracker keyup event


// ---------------------------------------------------------------------

// -------- EVENT handling for ITEM REMOVE button --------------------

'click .itemRemoveBtn' : function (event, tmpl) {

            event.preventDefault();

            var deletedItemID =  $(event.target).attr("itemID");

            if(typeof deletedItemID === "undefined")
            {

            }

            else{
            console.log(deletedItemID);

            Items.remove({"_id":deletedItemID});
            maxItemIndex.set( maxItemIndex.get()-1 );
            // setting indexCounter and new index order

                    var indexCounter=1;
                       $('.item-index').each(function(){
                               $(this).val(indexCounter);
                               indexCounter=indexCounter+1;

                              console.log(indexCounter);
                       });

            // END adjusting index

            // setting indexCounter on all db
                  $('.item-index').each(function(){
                              var currentItemID=$(this).attr("itemID");
                              var itemIndex = $(this).val();

                            //Items.update("DQ6SYBP75YhRSv88D", {$set: {"itemIndex":33 }} );
                            Items.update("currentItemID", {$set: {"itemIndex":itemIndex }} );
                       });

            // END adjusting index on db
          }

      },




//  ------------------ END event handlin for item remove button ------------


// ------------- event for discount calculations ------------------------------

    'keyup #discount, keyup .itemTracker' : function (event, tmpl) {

            var discount=$('#discount').val();
                if( isNaN(discount) )
                  {
                    discount=Number(0);

                  }
                else {
                  discount = Number( parseFloat (discount).toFixed(2) );

                }

                var discountedAmount= ( (currentSubtotal.get()*discount)/100 ).toFixed(2);

                currentDiscount.set( Number (discountedAmount) );

                // updating db
                var invoiceDBid=$('#invoiceDBid').val();
                        // discount
                    var dataDD = {
                                    invoiceDBid : invoiceDBid,
                                    discount : discount,
                                  }

              Meteor.call('updateDiscount', dataDD );

      },

  // -------------------- END event for discount calculation ---------------


  // ------------- event for tax calculations ------------------------------

      'keyup #tax, keyup .itemTracker, keyup #discount' : function (event, tmpl) {

              var tax=$('#tax').val();
                  if( isNaN(tax) )
                    {
                      tax=Number(0);
                    }
                  else {
                    tax = Number( parseFloat (tax).toFixed(2) );
                  }

                  //var taxedAmount= ( ( ( currentSubtotal.get()-currentDiscount.get() )*tax)/100 ).toFixed(2);
                    var taxedAmount=   ( ( currentSubtotal.get()*tax)/100 ).toFixed(2);

                currentTax.set( Number (taxedAmount) );

                // updating tax db
                var invoiceDBid=$('#invoiceDBid').val();
                var dataTX = {
                            invoiceDBid : invoiceDBid,
                            tax : tax,
                            }

                Meteor.call('updateTax', dataTX );

        },

    // -------------------- END event for tax calculation ---------------


    // ------------- event for total calculations ------------------------------

        'keyup #tax, keyup #discount, keyup .itemTracker' : function (event, tmpl) {

                var total;

                if( !isNaN(currentSubtotal.get()) )
                    {
                      total = Number( currentSubtotal.get() );
                    }

                if( !isNaN(currentDiscount.get()) )
                    {
                      //if user put any discount value then adjust total amount
                      total = Number( (currentSubtotal.get()-currentDiscount.get() ) );
                    }

                if( !isNaN(currentTax.get()) )
                    {
                      total = Number(  total+currentTax.get() );
                    }

                    if( !isNaN(total) )
                        {
                          total = Number( total.toFixed(2) );
                        }


                $('#total').val(total);


                // updating totalAmount db

                      var invoiceDBid=$('#invoiceDBid').val();
                      var data = {
                                invoiceDBid : invoiceDBid,
                                 totalAmount : total,
                      }

                Meteor.call('updateTotal', data );

                console.log("currentSubtotal : "+currentSubtotal.get()+" currentDiscount: "+currentDiscount.get()+" currentTax: "+currentTax.get() );

          },

      // -------------------- END event for Total calculation ---------------

      // ------------------- Event for Amount Paid & Due calculation --------

          'keyup #amountPaid, keyup .itemTracker ' : function (event, tmpl) {

                  var paidAmount=$('#amountPaid').val();

                      if( isNaN(paidAmount) )
                        {
                          paidAmount=0;
                        }
                      else {
                        paidAmount = Number( parseFloat (paidAmount).toFixed(2) );
                        }

                   var calculatedTotal = $('#total').val();

                       if( isNaN(calculatedTotal) )
                         {
                           calculatedTotal=0;
                         }
                       else {
                         calculatedTotal = Number( parseFloat (calculatedTotal).toFixed(2) );
                        }

                        var dueAmount= Number( (calculatedTotal - paidAmount).toFixed(2) ) ;

                    // displaying due amount

                    $('#dueBalance').val(dueAmount);


                    var invoiceDBid=$('#invoiceDBid').val();

                    //updating Paid amount to db

                    var data = {
                                    invoiceDBid : invoiceDBid,
                                     paidAmount : paidAmount,
                          }

                    Meteor.call('updatePaidAmount', data );

                   // updating due balance to db
                    var dataDA = {
                                invoiceDBid : invoiceDBid,
                                balanceDue : dueAmount,
                              }

                      Meteor.call('updateBalanceDue', dataDA );

            },

        // -------------------- END event for tax calculation ---------------



// --------------- END Event for Amount paid & due calculation -------------


// -------- event handler for updating notes & terms -----------------

//notes
'blur #notes' : function(event, tmpl){

      var invoiceDBid=$('#invoiceDBid').val();
      var notes=$('#notes').val();
      var data = {
                invoiceDBid : invoiceDBid,
                 notes : notes,
      }

Meteor.call('updateNotes', data );

},

//terms
'blur #terms' : function(event, tmpl){

      var invoiceDBid=$('#invoiceDBid').val();
      var terms=$('#terms').val();
      var data = {
                invoiceDBid : invoiceDBid,
                 terms : terms,
      }

Meteor.call('updateTerms', data );

},


// -------- END event handler for updating notes & terms -----------------


// -------------------------------------------------------------------
// ---------------------Retrieve Saved Invoice-----------------------
// -------------------------------------------------------------------



  'click #retrieve-invoice-btn': function (event, tmpl){

   event.preventDefault();

            var getUserID = $('#guest-user-id').val();

            var getInvoiceID = $('#prev-invoice-id').val();


            var result=Items.findOne(

                  { $and:[ {"invoiceID": getInvoiceID},

                          {"invoiceOwnerID": getUserID },

                          {"invoiceOwnerID" : {'$exists':true} } ] }

                    );

          if( result && result !== "null" && result !== "undefined" ){

                      $('#query-feedback').text('Invoice found and Retrieved! You can close this window now.');

                      $('#userID').text(result.invoiceOwnerID) ;

                      $('#invoiceID').text(result.invoiceID);

                      currentUserID.set(result.invoiceOwnerID);
                      currentInvoiceID.set(result.invoiceID);
                }

          else{

            //alert("nothing found");
            // $('#query-feedback').text('Nothing found! Please try again with valid ID!');
           $('#query-feedback').html('<strong>Nothing found! Invalid User ID or Invoice ID. </strong> <br> Please try again with valid ID!');
          }
          //  alert(getUserID);


 },

 // -------------------------------------------------------------------
 // ---------------------END event to Retrieve Saved Invoice-----------------------
 // -------------------------------------------------------------------




  });
}

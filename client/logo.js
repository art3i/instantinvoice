
Template.logo.rendered = function (){

  Tracker.autorun(function(){

   Meteor.subscribe("images");
  Meteor.subscribe("invoiceDB");

    })

} ;



Template.logo.helpers({


  getLogo: function () {

    var currentInvoiceID= $('#invoiceID').text();
    var currentUserID = $('#userID').text();
     
    var result=Invoice.findOne(

          { $and:[ {"invoiceID": currentInvoiceID },

                  {"invoiceOwnerID": currentUserID },

                  {"invoiceOwnerID" : {'$exists':true} } ] }

            );

  console.log("logo found "+result.logoID);

    return Images.find({"_id": result.logoID});



  },


});

// ---------------    ----------------------------------------




Template.logo.events( {

  'change #logoUpload': function(event, template) {

       var invoiceDBid=$('#invoiceDBid').val();

       FS.Utility.eachFile(event, function(file) {

         Images.insert(file, function (err, fileObj) {
             if (err){
                // handle error
                console.log("error uploading image");
             }

             else {
                // handle success depending what you need to do


               var data = {
                            "logoID": fileObj._id,
                            invoiceDBid : invoiceDBid,
                          };

               Meteor.call('updateLogo', data );

               console.log("done uploading image");

             }
           });
      });
    },

}) //end event handler

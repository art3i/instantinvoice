
// ----------------------------------------------------------------------------------------
// -------------- PUBLICATIONS ------------------------
// ----------------------------------------------------------------------------------------


Meteor.publish("invoiceDB" , function(userId){

// this will publish all types of self story regarding of public/ private filter to the timeline
// of the user.

// return StoryBook.find();  // uncomment for testing only;

       //return Invoice.find({"ownerID" : userId});
       return Invoice.find();


  });


  Meteor.publish("itemsDB" , function(userId){


        // return ItemCounter.find({"ownerID" : userId});
          return Items.find();


    });


// ---------------- Image meta data publications ------------------


Meteor.publish("images", function(){

                    return Images.find();

                });

// ------------------- END Images publications ------------------------





// ----------------------------------------------------------------------------------------
// -------------- END  PUBLICATIONS ------------------------
// ----------------------------------------------------------------------------------------

// ----------------------------------------------------------------------------------------
// -------------- access rules for GridFS ------------------------
// ----------------------------------------------------------------------------------------

Images.allow({
    download: function(userId, fileObj) {
        return true
    },

    update: function(userId, fileObj) {
       return true;
       },

    insert: function() {
        return true;
        },

    // remove: function(userId, fileObj) {
    //     return true;
    // },

})

Images.deny({
    remove: function(userId, fileObj) {
        return false;
    },


})



// ----------------------------------------------------------------------------------------
// -------------- END access rules for GridFS ------------------------
// ----------------------------------------------------------------------------------------






// ----------------------------------------------------------------------------------------
// -------------- Meteor METHODS ------------------------
// ----------------------------------------------------------------------------------------

Meteor.methods({


  'addInvoice' : function (data){

    Invoice.insert({

              invoiceOwnerID    : data.invoiceOwnerID,
              invoiceID         : data.invoiceID,

              ownerAddress      : data.ownerAddress,
              clientAddress     : data.clientAddress,

              logoID            : data.logoID,
              invoiceNumber     : data.invoiceNumber,
              dateCreated       : data.dateCreated,
              dueDate           : data.dueDate,
              discount          : data.discount,
              discountType      : data.discountType,
              tax               : data.tax,
              taxType           : data.taxType,

              subtotal          : data.subtotal,
              totalAmount       : data.totalAmount,
              paidAmount        : data.paidAmount,
              balanceDue        : data.balanceDue,

              notes             : data.notes,
              terms             : data.terms,



    });

        console.log("new invoice Created by : " + data.invoiceOwnerID );


  },

  // ------------- END adding new Invoice -------------

  'updateLogo' : function (data){

            Invoice.update(data.invoiceDBid, {$set: {

                          "logoID"  : data.logoID,
            }} );

  },


  'updateInvoiceNumber' : function (data){

            Invoice.update(data.invoiceDBid, {$set: {

                          "invoiceNumber"  : data.invoiceNumber,
            }} );

  },

  'updateOwnerAddress' : function (data){

            Invoice.update(data.invoiceDBid, {$set: {

                          "ownerAddress"  : data.ownerAddress,
            }} );

  },

  'updateClientAddress' : function (data){

            Invoice.update(data.invoiceDBid, {$set: {

                          "clientAddress"  : data.clientAddress,
            }} );

  },

  'updateDateCreated' : function (data){

            Invoice.update(data.invoiceDBid, {$set: {

                          "dateCreated"  : data.dateCreated,
            }} );

  },

  'updateDueDate' : function (data){

            Invoice.update(data.invoiceDBid, {$set: {

                          "dueDate"  : data.dueDate,
            }} );

  },

  'updateSubtotal' : function (data){

            Invoice.update(data.invoiceDBid, {$set: {

                          "subtotal"  : data.subtotal,
            }} );

  },

  'updateDiscount' : function (data){

            Invoice.update(data.invoiceDBid, {$set: {

                          "discount"  : data.discount,
            }} );

  },

  'updateTotal' : function (data){

            Invoice.update(data.invoiceDBid, {$set: {

                          "totalAmount"  : data.totalAmount,
            }} );

  },

  'updateTax' : function (data){

            Invoice.update(data.invoiceDBid, {$set: {

                          "tax"  : data.tax,
            }} );

  },

  'updatePaidAmount' : function (data){

            Invoice.update(data.invoiceDBid, {$set: {

                          "paidAmount"  : data.paidAmount,
            }} );

  },

  'updateBalanceDue' : function (data){

            Invoice.update(data.invoiceDBid, {$set: {

                          "balanceDue"  : data.balanceDue,
            }} );

  },

  'updateNotes' : function (data){

            Invoice.update(data.invoiceDBid, {$set: {

                          "notes"  : data.notes,
            }} );

  },

  'updateTerms' : function (data){

            Invoice.update(data.invoiceDBid, {$set: {

                          "terms"  : data.terms,
            }} );

  },

// ----------- END updating invoice


  'addItem' : function (data){

            Items.insert({

                          invoiceOwnerID  : data.invoiceOwnerID,
                            invoiceID     : data.invoiceID,
                            itemIndex     : data.itemIndex,
                            quantity      : data.quantity,
                            rate          : data.rate,
                            amount        : data.amount,
                            itemTitle     : data.itemTitle,

            });
            console.log("new item db created with index : "+data.itemIndex);


  },


// -------- Update Items on each data changes

'updateItem' : function (data){

          Items.update(data.itemID, {$set: {

                        "invoiceOwnerID"  : data.invoiceOwnerID,
                          "invoiceID"     : data.invoiceID,
                          "itemIndex"     : data.itemIndex,
                          "quantity"      : data.quantity,
                          "rate"          : data.rate,
                          "amount"        : data.amount,
                          "itemTitle"     : data.itemTitle,

          }} );
          console.log("item db updated with index : "+data.itemIndex);



},


// --------------- Remoing item on remove button click event -----------

'removeItem' : function (itemID){

          Items.remove({"_id":itemID});

          console.log("item removed  : "+itemID);



},

'updateItemIndex' : function (data){

          Items.update(data.itemID, {$set: {
                          "itemIndex"     : data.itemIndex,
          }} );
          console.log("item  index updated of : "+data.itemID);

},

// -----------------------


    'clearAllDB' : function (){

// this is to clear all db collections during development.
//here ~ remove({}) ~ this {} is needed to remove all documents

// comment out this method before deployment or use admin only authintication like if user==admin checking

//   Meteor.call('clearAllDB');

      // Meteor.users.remove({});
       Invoice.remove({});

       Images.remove({});
       Items.remove({});


          console.log( " removed ... ");


    },


// ------------- method for image upload to GridFS filesystem----------------

'saveImage' : function (data){

var data=data;
  console.log("miaw: "+data.file);

         Images.insert(data.file, function (err, fileObj) {
             if (err){
                // handle error
                console.log("error uploading image");
             }

             else {

                var imagesURL = {
                   "profile.image": "/cfs/files/images/" + fileObj._id
                    };

                  Meteor.users.update(data.userId, {$set: imagesURL});

             }
           });




      console.log("Image saved to GridFS local storage : " + data.authorName );

},



});


// ----------------------------------------------------------------------------------------
// -------------- END  Meteor METHODS ------------------------
// ----------------------------------------------------------------------------------------
